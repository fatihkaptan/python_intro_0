# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 20:09:56 2020

@author: Kaptan
"""

lights = ["red","yellow","green"]

currentLight = lights[0]

print(currentLight)

if currentLight=="red":
    print("stop!")
    
if currentLight=="yellow":
    print("ready")

if currentLight=="green":
    print("go!")    
    