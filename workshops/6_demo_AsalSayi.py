# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 14:45:55 2020

@author: Kaptan
"""

"""asal sayı kuralına bakalım:
    2 den başlar, 1 ve kendinden başka böleni olmayan sayılar""" #2,3,5,7,11...
 
    
sayi = int(input("Sayı Giriniz: "))

#11 için algoritma geliştirirsek: 

#print(7 % 2)  #yüzde işareti kalanı verir..
asalMi = True

for x in range(2,sayi):
    if (sayi % x) == 0:
        asalMi= False
        break
    
if asalMi == True: # if asalMi: defaultu true ' dur yani true yazmasakta olur
    print("Asal")
else:
    print("Asal Değil.")