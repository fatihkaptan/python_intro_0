# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 17:52:04 2020

@author: Kaptan
"""

#%%
class Matematik:
    def __init__(self):
        print("çalıştı")
    def topla(self,sayi1,sayi2):
        return sayi1+sayi2
    
    def cikar(self,sayi1,sayi2):
        return sayi1-sayi2
    
    def carp(self,sayi1,sayi2):
        return sayi1*sayi2
    
    def bol(self,sayi1,sayi2):
        return sayi1/sayi2
        
#self kullanımı


#class Matematik:
#    def __init__(self,sayi1,sayi2):
#        self.sayi1 = sayi1
#        self.sayi2 = sayi2
#    def topla(self):
#        return self.sayi1 + self.sayi2
#    
#    def cikar(self):
#        return self.sayi1 - self.sayi2
#    
#    def carp(self):
#        return self.sayi1 * self.sayi2
#    
#    def bol(self):
#        return (self.sayi1) / (self.sayi2)
#    
mat = Matematik()
print(mat.topla(10,2))
print(mat.cikar(10,2))
print(mat.carp(10,2))    
print(mat.bol(10,2))
    
#%% Property

class Person:
    def __init__(self,firstName,lastName,age):
        self.firstName = firstName
        self.lastName = lastName
        self.age = age


person1 = Person("Fatih","Kaptan",21)
print(person1.firstName)
print(person1.lastName)
print(person1.age)

class Worker(Person):
    def __init__(self,salary):
        self.salary = salary
        
        
class Customer(Person):
    def __init__(self,creditCardNumber):
        self.creditCarNumber = creditCardNumber
        
        
ahmet = Worker(1234)
print(ahmet.salary)

mehmet = Customer("093184 31299 3219009")
print(mehmet.creditCarNumber)

#%%


































