# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 19:40:05 2020

@author: Kaptan
"""

#set union ile iki setin tekrar eden girdilerini tekrarlatmadan alırız..

setA = {1,2,3,4,5,}
setB = {1,3,4,6,7,8}

print(setA | setB)
print(setA.union(setB))     #ikisi de kullanılabilir

### ortak girdileri bulmak için intersection-and işareti kullanılır

print(setA & setB)
print(setA.intersection(setB))

### tekrarlayınları(kesişimi) atmak için difference komutu kullanılır.

print(setA - setB)
print(setA.difference(setB))

print(setB - setA)
print(setB.difference(setA))

### Her iki kümeyi alan ama tekrarlayanları atmak için symmetric difference kullanılırız.
print(setA ^ setB)
print(setA.symmetric_difference(setB))