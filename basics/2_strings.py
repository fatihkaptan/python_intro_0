# -*- coding: utf-8 -*-
"""
Created on Sat Jan 18 22:15:08 2020

@author: Kaptan
"""

message ="Hello World!"

"""subString yani metni parçalak"""

subString1 = message[2] #0 1 2. harf
subString2 = message[2:5] #2'den 5'e kadar
subString3 = message[2:]  #2'den sona kadar
subString4 = message[:2] #2'ye kadar
#print(message[2])

""" metnin uzunluğunu bulma fonksiyonu = len()"""

lenFunc = len(message)

""" lower - upper (metnin büyük küçük harf değiştirme"""

print(message.upper()) 
print(message.lower())


""" replace fonskiyonu (karakter değşitirme)"""

print(message.replace("e","3"))

""" Split ve Strip fonksiyonları"""

info = "Fatih Kaptanoğlu 21 İstanbul"
info2 = "Fatih;Kaptanoğlu;21;İstanbul"

print(info)
print(info.split()) #boşlukları görünce parçalar (varsayılan)
print(info2.split(";")) #içine girdiğimiz karaktere göre de ayırır
print("Soyadı: " + info.split()[1])
print("Yaş:" + " " + info.split()[2]) #bu şekilde indexleyebiliriz


"""input (kulanıcıdan veri alma)"""

ad = input("Adınız: ")

yas = input("Yaşınız: ")
dogum = 2020-int(yas)
print("Doğum: ")
print(dogum)





























