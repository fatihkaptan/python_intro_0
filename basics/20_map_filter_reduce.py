# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 22:43:53 2020

@author: Kaptan
"""

sayilar = [1,2,3,4,5]

kareler= []

for sayi in sayilar:
    kareler.append(sayi*sayi)
    
"""mapping yöntemi ile """

kareler2 = list(map(lambda x: (x**2),sayilar))


"""filter yöntemi"""

kareler3 = list(filter(lambda y: y>2 ,sayilar))

""" reduce fonksiyonu """

from functools import reduce
faktoriyel = reduce(lambda x,y: x*y ,sayilar)




print(kareler)
print(kareler2)
print(kareler3)
print(faktoriyel)