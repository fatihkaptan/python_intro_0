# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 15:00:52 2020

@author: Kaptan
"""
#%%
def selamVer(isim = "Dostum"):  #bu kısma girilen parametre ve eşitlenen default parametre girilir.
    print("Merhaba " + isim)
    
selamVer("Fatih")
selamVer()

def selamVer2(isim = "Dostum",soyIsım = ""):
#def selamVer2(isim = "Dostum",soyIsım): #burada dikkat etmemiz gereken default değer son parametreler olacak

    print("Merhaba " + isim + soyIsım)
    
selamVer2("Fatih ","Kaptan")  #burada dikkat etmemiz gereken default değer son parametreler olacak
selamVer2()
#%%  #return eden fonskiyonlar
#sehir = "Ankara"
#sonuc = sehir.upper()

def dikUcgenAlan(a,b):
    return a*b/2

dikUcgenAlan(5,4)  #bunu çalıştıırsak değişken atamaz bunu atamamız lazım..
alan = dikUcgenAlan(5,4)
print("alan: " + str(alan))
#%%  #lambda fonskiyon

dikUcgenAlan2 =  lambda a,b : a*b/2  #lambda'da tek satırda returnlu fonskiyon atanır

print(dikUcgenAlan2(3,8))

x= dikUcgenAlan2  #fonskiyon atama
print(x(6,90))


















