# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 22:35:56 2020

@author: Kaptan
"""

sehirler=["Ankara","İstanbul","İzmir"]

iteratorum = iter(sehirler)

print(next(iteratorum))
print(next(iteratorum))
print(next(iteratorum))
#print(next(iteratorum))

for sehir in sehirler:
    print(sehir)