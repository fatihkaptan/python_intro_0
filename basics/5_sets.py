# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 19:23:37 2020

@author: Kaptan
"""
#set tipinde girdilerin belirli bir yeri yoktur sistem rastgele atar
studentSet = {"Fatih","Mesut","Melis"}
print(studentSet)

for student in studentSet:  #bu yöntemle index sıralı listesine ulaşabiliriz..
    print(student)

print("Melis" in studentSet)  #bu şekilde setin içinde olduğunu yazdırabiliriz.

if "Melek" in studentSet:
    print("listede var")
    
else:
    print("Melek listede yok")

#----------------------------------

studentSet.add("Ali")  #tek girdi ekleyebiriz
print(studentSet)

studentSet.update(["Merve","Muharrem","Recep"]) #liste şeklinde ekleyebiliriz
print(studentSet) #listede karışık yere atıyor hep asla sıralı değil


studentSet.remove("Recep")
print(studentSet)

studentSet.discard("Serap")     # remove ile discardın farkı: discard olmayanı
                                # silmeye çalışsak dahi hata vermez
print(studentSet)

x= studentSet.pop()  #dikkat pop() rastgele siler
print(studentSet)

y= studentSet.clear()  #içini temizler
print(y)

del studentSet
print(studentSet)   #uçurur seti





