# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 21:15:44 2020

@author: Kaptan
"""

import sqlite3

connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz

cursor = connection.execute("select * from customers") #sorgu gönderme(* hepsi demek)

for row in cursor:
    print("First Name:",row[1],row[2])
#    print("Last Name:",row[2])
    

connection.close()
#%% şartlı çekme

import sqlite3

connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz

cursor = connection.execute("""select * from customers 
                            where city='Prague' or city='Berlin'  
                            order by FirstName""") #sıralama şart
#burada tek tırnak içine almak gerekiyr yoksa kolon hatası verir
#üç çift tırnak aşağı kayırmak için kullanılır
#order by ascending(defoult) ve descending(tersi)
#
for row in cursor:
    print("First Name:",row[1],row[2])
#    print("Last Name:",row[2])
    

connection.close()

#%% group by ve having

import sqlite3

connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz

cursor = connection.execute("""select city,count(*) from customers 
                            group by city
                            having count(*)>1
                            order by count(*) desc
                                                                """)
#şehre göre grupladım
#tersten yazdurdum desc))
#having şartı ise 1'den büyükleri yazdırdı

for row in cursor:
    print("City:",row[0],"Count:",row[1])

    

connection.close()

#%% like komutu 

import sqlite3

connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz

cursor = connection.execute("""select FirstName,LastName  
                            from customers 
                            where FirstName like'%a%'
                                                                """)
#like '%a%' komutu ile fisrtname kolonu içinde a harfi olan verileri çekeriz
# '%a' = a ile bitenler
# 'a%' = a ile başlayanlar
for row in cursor:
    print("person:",row[0],row[1])

    

connection.close()

#%%  insert operasyonları (yukarısı hep select)

import sqlite3
def insert_func():
    connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz
    
    connection.execute("""insert into customers (FirstName,LastName,City,Email) 
                            values('Fatih','Kaptan','Konya','kaptan587@gmail.com')
                                                                    """)
    connection.commit()
    connection.close()
    print()
insert_func()

#%% GÜNCELLEME OPERATÖRÜ (konyaları istanbul yapalım)

import sqlite3

def update_func():
    connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz
    
    connection.execute("""update customers set city = 'İstanbul'
                       where city = 'Konya'
                                                                    """)
    connection.commit()
    connection.close()


update_func()

#%% SİLME OPERATÖRÜ DEL

import sqlite3

def delete_func():
    connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz
    
    connection.execute("""delete from customers 
                       where city='Istanbul' or city ='İstanbul'
                                                                    """)
    connection.commit()
    connection.close()


delete_func()

#%% JOİN OPERATÖRLERİ (ilişkili iki veriyi çekme)
"""kod execute sql'de yazılı hali : (sanatçının albümlerini 
                                    farklı veritabanlarından çekiyoruz)

    select albums.Title, artists.Name from artists inner join albums
    on artists.ArtistId = albums.ArtistId  """
    
import sqlite3

def join_func():
    connection = sqlite3.connect("chinook.db") #veritabanını bağlıyoruz
    
    cursor = connection.execute("""select albums.Title, 
                       artists.Name from artists 
                       inner join albums
                       on artists.ArtistId = albums.ArtistId 
                                                                    """)
    for row in cursor:
        print("Title: ",row[0],"Name:",row[1])
    connection.close()

join_func()









