# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 17:37:11 2020

@author: Kaptan
"""

ogrenciler = ["Fatih","Selena","Hades"]

print(ogrenciler[1])
ogrenciler.append("Yüce Honos")  #append ilave etmek anlamına gelir
ogrenciler.remove("Selena")  # remove komutuyla siler
ogrenciler[0] = "DjDikkat"  #listenin girdisini değiştirir
print(ogrenciler)

"""diger liste olusturma bicimi"""

sehirler = list(("Ankara","İstanbul"))
sehirler.append("Ankara")
print(len(sehirler))

"""DİĞER FONKSİYONLAR"""

#print(sehirler.clear())  #listeyi temizler
print("Ankara sayısı = " + str(sehirler.count("Ankara")))  #değerin sayısını verir
print("Ankara indexi = " + str(sehirler.index("Ankara"))) #girdinin index sırasını verir

print(sehirler.pop(1))  #pop fonskiyonu girilen indexi listeden atar yerine yeni girdi kayar
print(sehirler.insert(0,"Kars")) # pop'un tersi girdiyi yerleştirir listeyi öteler
print(sehirler.reverse()) #listeyi ters çevirir.
print(sehirler)

"""dikkat"""
sehirler3 = sehirler.copy()
sehirler2= sehirler
sehirler2[0] = "İzmir"
print(sehirler2)
print(sehirler)
print(sehirler3)
"""buradaki nokta siz listeyi farklı değişken olarak çekip 
değiştirmek isterken aslında iki listeyi de değiştiriyorsunuz
bu durumda .copy() komutunu kullanmamız işi kolaylaştırır..."""

sehirler.extend(sehirler3)    #listeye girilen listeyi ekler...
print(sehirler) 
(sehirler.sort())  #listeyi alfabeye göre sıralar
print(sehirler)

""">>>>tuple gectim""" 




























