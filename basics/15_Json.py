# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 19:30:03 2020

@author: Kaptan
"""
import json


data = '{"firstName":"Fatih","lastName":"Kaptan"}'

y = json.loads(data)

print(type(y))
print(y["firstName"])
print(y["lastName"])

customer = {
        "ad":"fatih",
        "soyad":"kaptan",
        "email":"kaptan587@gmail.com"
        }

customerJson = json.dumps(customer)
print(customer)
print(type(customer))

print(json.dumps("Engin"))