# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 18:10:11 2020

@author: Kaptan
"""
#tuple değişmeyen kümedir read-only
tupleListe= (2,4,6,"Ankara",(1,2,3),()) # küme tarzı liste içinde liste yapılabilir
liste= [2,4,6,"Ankara",[1,2,3],[]]
print(tupleListe)
print(liste)

print(tupleListe[-1])
print(liste[-2])   # eksi olarak idexlersek geriden saymaya başlar -1 sonunca -2 sondan bi önce...