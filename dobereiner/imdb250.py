# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 17:23:50 2020

@author: Kaptan
"""
#%%
import requests
from bs4 import BeautifulSoup

text_file = open("Output.txt", "w")



url = 'https://www.imdb.com/chart/top/'
r= requests.get(url)
print(r.ok)

soup = BeautifulSoup(r.content,"html.parser")

gelen_veri = soup.find_all("table",{"class":"chart full-width"})

#print(gelen_veri[0].contents)
film_tablosu = (gelen_veri[0].contents)[len(gelen_veri[0].contents)-2]
#print(len(film_tablosu))
film_tablosu = film_tablosu.find_all("tr")
for film in film_tablosu:
    film_basligi = film.find_all("td",{"class":"titleColumn"})
    film_ismi = film_basligi[0].text
    film_ismi = film_ismi.replace("\n","")   
    film_puani = film.find_all("td",{"class":"ratingColumn imdbRating"})[0].text
    film_puani = film_puani.replace("\n","")
    print(film_ismi[6:10]+"  "+film_puani+"   "+film_ismi[13:])
    
    text_file.write(film_ismi[6:10])
    text_file.write("  ")
    text_file.write(film_puani)
    text_file.write("  ")
    text_file.write(film_ismi[13:])
    text_file.write("\n")

    
text_file.close() 
